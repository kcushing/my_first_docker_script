#!/usr/bin/env bash

##
# name of the webserver container
docker_container_name="some-nginx"

##
#\brief Start the webserver
run_nginx()
{
	## The docker rm command may generate an error as 
	# we are not checking to see if it is already been removed
	docker rm ${docker_container_name}
	# The docker run command for a host mounted directory
	# See https://hub.docker.com/_/nginx/
	docker run \
		--name ${docker_container_name} \
		-v ${HOME}/Documents/www:/usr/share/nginx/html:ro \
		-p 80:80 \
		-d \
		nginx	

	# Explanation
	# docker run: Run a docker image ( image is nginx at the end )
	# --name Instead of random name, your image will always be called some-nginx when you do "docker ps -a"
	# -v, This is the magic that mounts your phyical laptop harddisk folder to the container virtual folder.
	# -d, make the container run in the background so it does not die on an exit.
	# -p, internet port assignment. host:container port number. later on change to 8080:80
	# nginx is the image name as mentioned above.

}

##
#\brief Stop the webserver
stop_nginx()
{
	docker stop ${docker_container_name}
}

##
#\brief This will list all the running and non-running containers
#\note ps, see man ps
list_containers()
{
	docker ps -a
}

##
#\brief this will list all the downloaded docker images
list_images()
{
	docker images
}

##
#\brief This will remove all the stopped containers
prune_containers()
{
	docker container prune
}
